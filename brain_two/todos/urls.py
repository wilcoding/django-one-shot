from django import views
from django.urls import path
from todos.views import todo_list_detail, todo_lists, CreateList, edit_todo_list, delete_todo_list, todo_item_create


urlpatterns = [
    path("", todo_lists, name="todo_list_list"),
    path("<int:id>", todo_list_detail, name="todo_list_detail"),
    path('create/', CreateList.as_view(), name='todo_list_create'),
    path('<int:id>/edit/', edit_todo_list, name='edit_todo_list'),
    path('<int:id>/delete/', delete_todo_list, name= 'delete_todo_list'),
    path('items/create/', todo_item_create.as_view(), name='todo_item_create' )

    ]
