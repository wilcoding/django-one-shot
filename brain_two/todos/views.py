from django.shortcuts import render, HttpResponse,redirect,get_object_or_404
from .models import TodoList, TodoItem
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy
from todos.forms import TodoListForm

# Create your views here.]

def todo_lists(request):
    todo_list = TodoList.objects.all()
    context = {'todo_lists': todo_list}
    return render(request, 'todo/todo_list.html', context)


def todo_list_detail(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    return render(request, 'todo/detail.html', {'todo_list': todolist})


class CreateList(CreateView):
    model = TodoList
    fields = ['name']
    success_url = reverse_lazy('todo_list_list')
    template_name= 'todo/create.html'


def edit_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            # todo_list.user = request.user
            # todo_list.save()
            return redirect('todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {
        "form": form
    }
    return render(request, 'todo/edit.html', context)


def delete_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todo_list_list')
    else:
        return render(request, 'todo/delete.html')


class todo_item_create(CreateView):
    model = TodoItem
    fields = ['task', 'due_date','is_completed', 'todo_list']
    template_name = 'todo/createitem.html'
    success_url = reverse_lazy('todo_list_list')
