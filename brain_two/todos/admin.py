from django.contrib import admin
from .models import TodoList,TodoItem

# Register your models here.


class TodoListAdmin(admin.ModelAdmin):
    list_display = ("name", "id")


class TodoItemAdmin(admin.ModelAdmin):
    list_display = ("task", "due_date")

admin.site.register(TodoItem, TodoItemAdmin)
admin.site.register(TodoList, TodoListAdmin)
